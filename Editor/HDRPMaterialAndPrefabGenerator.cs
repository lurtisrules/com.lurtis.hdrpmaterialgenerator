﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Assets.Editor
{
    public class HDRPMaterialAndPrefabGenerator : EditorWindow
    {
    
        private const string ButtonLabel = "Create HDRP Materials And Prefabs";
        private const string TitleLabel = "HDRP Material and Prefab Generator";
        private const string MenuItemName = "Lurtis4Unity/HDRP Material and Prefab Generator";
        private const string PathLabel = "Path: ";
        private const string SearchLabel = "...";
        private const string SelectFolderButtonLabel = "Select Folder";

        private const string AssetsFolderName = "Assets";

        private const string MaterialPrefix = "M_";
        private const string MaterialExtension = ".mat";
        private const string PrefabPrefix = "P_";
        private const string PrefabExtension = ".prefab";


        private const string BaseColorSufix = "_BC";
        private const string MaskSufix = "_MK";
        private const string NormalSufix = "_N";

        private const string BaseColorShaderParameterName = "_BaseColorMap";
        private const string MaskShaderParameterName = "_MaskMap";
        private const string NormalShaderParameterName = "_NormalMap";

        private const string ErrorMessage = "ERROR.";
        private const string InvalidSelectedPathErrorMessage = "Selected path not valid.";
        private const string MissingFbxErrorMessage = "Cannot find FBX at Path:";
        private const string TexturesAmountErrorMessage = "Current path has not 3 textures:";
        private const string MissingTexturesErrorMessage = "Cannot find BC, MK and/or N texture at Path:";
        private const string InvalidAbsolutePathErrorMessage = "Invalid Absolute Path:";

        private const string DefaultShader = "HDRP/Lit";

        private string _path = "";
        private float _currentMeshPositionXAxis;
	
        [MenuItem(MenuItemName)]
        public static void ShowWindow()
        {
            GetWindow(typeof(HDRPMaterialAndPrefabGenerator));
        }
	
        private void OnGUI()
        {
            GUILayout.Label(TitleLabel, EditorStyles.boldLabel);
            EditorGUILayout.Space(10.0f);
            RootFolderSelector();
            EditorGUILayout.Space(10.0f);
            GenerateAllMaterialsAndPrefabsButton();
        }
	
        private void RootFolderSelector()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(PathLabel, this._path);
		
            if (GUILayout.Button(SearchLabel, GUILayout.Width(50.0f)))
            {
                this._path = EditorUtility.OpenFolderPanel(SelectFolderButtonLabel, "", "");
                this.Repaint();
            }

            EditorGUILayout.EndHorizontal();
        }
	
        private void GenerateAllMaterialsAndPrefabsButton()
        {
            if (GUILayout.Button(ButtonLabel))
            {
                if(!IsPathValid()) return;
                this._currentMeshPositionXAxis = default;
                var subDirectories = Directory.GetDirectories(this._path).ToList();
                subDirectories.ForEach(GenerateMaterialAndPrefabAt);
            }
        }
	
        private bool IsPathValid()
        {
            if (string.IsNullOrEmpty(this._path) || !Directory.Exists(this._path))
            {
                Debug.LogError(InvalidSelectedPathErrorMessage);
                return false;
            }

            return true;
        }
	
        private void GenerateMaterialAndPrefabAt(string path)
        {
            var modelAbsolutePath = Directory.GetFiles(path, ".", SearchOption.AllDirectories).First(x => Regex.IsMatch(x, @".fbx$"));
            if (string.IsNullOrEmpty(modelAbsolutePath))
                throw new Exception($"{ErrorMessage}. {MissingFbxErrorMessage} {path}");

            var modelName = ModelNameFrom(modelAbsolutePath);
            var modelRelativePath = FileRelativePathFrom(modelAbsolutePath);

            CreatePrefabAndMaterialAt(path, modelRelativePath, modelName);
        }

        private string ModelNameFrom(string modelPath)
        {
            var modelName = Path.GetFileName(FileRelativePathFrom(modelPath)).Remove(0, 3);
            modelName = modelName.Substring(0, modelName.Length - Path.GetExtension(modelName).Length);
            modelName = modelName.Substring(0, modelName.Length - Path.GetExtension(modelName).Length);

            return modelName;
        }

        private string FileRelativePathFrom(string absolutePath)
        {
            if (absolutePath.StartsWith(Application.dataPath))
            {
                var pos = absolutePath.IndexOf(AssetsFolderName, StringComparison.Ordinal);
                if (pos >= 0)
                {
                    return absolutePath.Remove(0, pos);
                }
            }

            throw new ArgumentException($"{ErrorMessage} {InvalidAbsolutePathErrorMessage}" + absolutePath);

        }

        private void CreatePrefabAndMaterialAt(string path, string modelRelativePath, string modelName)
        {
            var fbxAsset = (GameObject) AssetDatabase.LoadAssetAtPath(modelRelativePath, typeof(GameObject));
            var fbxInstancedAsGameObject = Instantiate(fbxAsset);
            var modelMaterial = CreateNewMaterialAt(path, modelName);
            fbxInstancedAsGameObject.GetComponentsInChildren<MeshRenderer>().ToList()
                .ForEach(x => x.materials = new[] {modelMaterial});
            var fbxPrefabInstanced = Instantiate(PrefabUtility.SaveAsPrefabAsset(fbxInstancedAsGameObject,
                FileRelativePathFrom(path) + $"/{PrefabPrefix}{modelName}{PrefabExtension}"));
            DestroyImmediate(fbxInstancedAsGameObject);
            fbxPrefabInstanced.transform.position += new Vector3(_currentMeshPositionXAxis++, 0.0f, 0.0f);
        }

        private Material CreateNewMaterialAt(string path, string materialName)
        {
            var texturesPath = Directory.GetFiles(path, ".", SearchOption.AllDirectories).ToList().Where(x => Regex.IsMatch(x, @".jpg|.png|.gif|.jpeg|.tif$")).ToList();
            if (texturesPath.Count != 3)
            {
                throw new Exception($"{ErrorMessage}. {TexturesAmountErrorMessage} {path}");
            }

            Texture2D TextureBy(string sufix) => TextureBySufixAt(texturesPath, sufix);
            var baseColorTexture = TextureBy(BaseColorSufix);
            var maskTexture = TextureBy(MaskSufix);
            var normalTexture = TextureBy(NormalSufix);

            var materialRelativePath = FileRelativePathFrom(path) + $"/{MaterialPrefix}{materialName}{MaterialExtension}";

            return CreateMaterialBy(baseColorTexture, maskTexture, normalTexture, materialRelativePath);
        }

        private Texture2D TextureBySufixAt(List<string> directoryPath, string sufix)
        {
            var baseColorTexturePath = FileRelativePathFrom(directoryPath.FirstOrDefault(x => Path.GetFileName(x).Contains(sufix)));
            if (string.IsNullOrEmpty(baseColorTexturePath))
            {
                throw new Exception(
                    $"{ErrorMessage}. {MissingTexturesErrorMessage} {directoryPath.FirstOrDefault()}");
            }

            return (Texture2D)AssetDatabase.LoadAssetAtPath(baseColorTexturePath, typeof(Texture2D));
        }

        private static Material CreateMaterialBy(Texture2D baseColorTexture, Texture2D maskTexture, Texture2D normalTexture,
            string relativePath)
        {
            AssetDatabase.CreateAsset(new Material(Shader.Find(DefaultShader)), relativePath);
            var newMaterial = (Material) AssetDatabase.LoadAssetAtPath(relativePath, typeof(Material));
            newMaterial.SetTexture(BaseColorShaderParameterName, baseColorTexture);
            newMaterial.SetTexture(MaskShaderParameterName, maskTexture);
            newMaterial.SetTexture(NormalShaderParameterName, normalTexture);
            return newMaterial;
        }

    }
}
