# HDRP Material and Prefab Generator


## What is HDRP Material and Prefab Generator? 
> HDRP Material and Prefab Generator is a tool to automatically create HDRP materials from your three basic textures and prefabs with their assigned HDRP/Lit material.


## Quickstart

Tested in Unity 2019.4

HDRP Material and Prefab Generator works as follows: the user must organize the files so that for each model / material has a folder with the name of the model itself and that contains the three textures and the 3D mesh. The nomenclature to follow is:
T_Model_BC (Texture Base Color)
T_Model_MK (Mask Texture)
T_Model_N (Normal Texture, which must be set manually in Unity as Normal type texture)
SM_Model (FBX / 3D Mesh)

Each folder containing these four files must be stored in another parent folder that can be named in any way. This parent folder is the one that the user must select from the window provided by this tool to automate the generation of materials and prefabs.

## Example

![Example Image](./example.jpg)

## Installation

1. Open ```Packages/manifest.json``` and add at the end of the file:
    ```
    "com.lurtis.hdrpmaterialgenerator":  "https://bitbucket.org/lurtisrules/com.lurtis.hdrpmaterialgenerator.git"
    ```
    
    Also, you cand add the project at "Window > Package Manager > "+" > Add Package From Git URL" and paste the next URL: https://bitbucket.org/lurtisrules/com.lurtis.hdrpmaterialgenerator.git

2. Open the window at ```Lurtis4Unity / HDRP Material and Prefab Generator```


3. Select the parent folder you want and click on ```Create HDRP Materials And Prefabs``` button.
